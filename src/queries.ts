import { gql } from "@apollo/client";

export const GET_POSTS = gql`
  query GetPosts($count: Int) {
    allPosts(count: $count) {
      published
      createdAt
      author {
        id
        firstName
        lastName
        avatar
      }
      likelyTopics {
        label
      }
    }
  }
`;

export const GET_USER = gql`
  query GetUser($id: ID!) {
    User(id: $id) {
      id
      firstName
      lastName
      avatar
    }
  }
`;
