function filterUserPostsByDate(posts: any, year: number, month: number) {
  return posts.filter((post: any) => {
    const createdAt = new Date(parseInt(post.createdAt));
    if (month === 0 && year === 0) return true;
    else if (month === 0) return createdAt.getFullYear() === year;
    else if (year === 0) return createdAt.getMonth() === month - 1;
    else {
      return (
        createdAt.getFullYear() === year && createdAt.getMonth() === month - 1
      );
    }
  });
}

export default filterUserPostsByDate;
