import React, { useReducer, createContext } from "react";

export const DataContext = createContext<any>({});

const initialState: any = {
  count: 1000,
  posts: [],
  users: [],
};

interface Action {
  type: string;
  payload: any;
}

const reducer = (state: any, action: Action) => {
  switch (action.type) {
    case "SET_COUNT":
      if (state.count !== action.payload) {
        return {
          posts: [],
          users: [],
          count: action.payload,
        };
      } else {
        return { ...state, count: action.payload };
      }
    case "SET_POSTS":
      return {
        ...state,
        posts: action.payload,
      };
    case "SET_USERS":
      return {
        ...state,
        users: action.payload,
      };
    default:
      throw new Error();
  }
};

function DataContextProvider(props: any) {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <DataContext.Provider value={[state, dispatch]}>
      {props.children}
    </DataContext.Provider>
  );
}

export default DataContextProvider;
