import filterUserPostsByDate from "../utils";

function useGetUserActivity(
  posts: any,
  userId: string,
  year: number,
  month: number
) {
  posts = posts.filter((post: any) => post.author.id === userId);
  const allUserPosts = filterUserPostsByDate(posts, year, month);
  const allUserPublishedPosts = allUserPosts.filter(
    (post: any) => post.published === true
  );
  const allUserPostsCount = allUserPosts.length;
  const userPublishedPostsCount = allUserPublishedPosts.length;

  let userActivityData = [];
  for (let activityMonth = 1; activityMonth <= 12; activityMonth++) {
    const UserPostsInMonth = filterUserPostsByDate(posts, year, activityMonth);

    userActivityData.push({
      activityMonth: `${year}-${("0" + activityMonth).slice(-2)}-01`,
      userPostsCount: UserPostsInMonth.length,
    });
  }

  return [allUserPostsCount, userPublishedPostsCount, userActivityData];
}

export default useGetUserActivity;
