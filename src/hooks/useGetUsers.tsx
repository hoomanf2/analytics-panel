function useGetUsers(posts: any) {
  let users: any = [];
  posts.forEach((post: any) => {
    const isUserExist =
      users.filter((user: any) => user.id === post.author.id).length !== 0;
    if (!isUserExist) users.push(post.author);
  });

  return users;
}

export default useGetUsers;
