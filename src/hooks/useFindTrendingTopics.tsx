import filterUserPostsByDate from "../utils";

function useFindTrendingTopics(
  posts: any,
  year: number,
  month: number,
  userId: string | null
) {
  let topics: any = [];

  if (userId) {
    posts = posts.filter((post: any) => post.author.id === userId);
  }

  filterUserPostsByDate(posts, year, month).forEach((post: any) => {
    const label = post.likelyTopics[0].label;
    const labelIndex = topics.findIndex((item: any) => item.label === label);
    if (labelIndex !== -1) {
      topics[labelIndex].repetition += 1;
    } else {
      topics = [...topics, { label, repetition: 1 }];
    }
  });

  return topics;
}

export default useFindTrendingTopics;
