import React from "react";
import DataContextProvider from "./context/DataContext";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import UserProfile from "./pages/UserProfile";
import NotFound from "./pages/NotFound";

import "./App.css";

function App() {
  return (
    <DataContextProvider>
      <Router>
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route path="/user/:userId" component={UserProfile} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    </DataContextProvider>
  );
}

export default App;
