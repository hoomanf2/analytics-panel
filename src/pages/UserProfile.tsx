import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { DataContext } from "../context/DataContext";
import { useQuery } from "@apollo/client";
import UserActivity from "../components/UserActivity";
import TrendingTopics from "../components/TrendingTopics";
import NotFound from "../pages/NotFound";
import { GET_USER } from "../queries";
import { Container, Spinner, Alert, Card, Button } from "react-bootstrap";

function UserProfile({ match }: any) {
  let history = useHistory();

  const {
    params: { userId },
  } = match;

  const [state] = useContext(DataContext);

  if (state.posts.length === 0) history.push("/");

  const { loading, error, data } = useQuery(GET_USER, {
    variables: { id: userId },
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  if (loading) {
    return (
      <Container className={"mt-5"}>
        <Spinner className={"loading"} animation="border" variant="primary" />
      </Container>
    );
  }

  if (error) {
    return (
      <Container className={"mt-5"}>
        <Alert variant={"danger"}>Something went wrong!</Alert>
      </Container>
    );
  }

  if (data) {
    const user = data.User;
    return (
      <Container>
        <Card className={"mt-5 mb-4"}>
          <Card.Body className={"d-flex"}>
            <img
              className={"avatar"}
              src={user.avatar}
              alt={`${user.firstName} ${user.lastName}`}
            />
            <h3 className={"name"}>{`${user.firstName} ${user.lastName}`}</h3>
          </Card.Body>
        </Card>

        <Button
          className={"mb-4"}
          variant="dark"
          onClick={() => history.goBack()}
        >
          Back to Dashboard
        </Button>
        <Card className={"mb-4"}>
          <Card.Body>
            <Card.Title>User Activity</Card.Title>
            <UserActivity posts={state.posts} userId={user.id} />
          </Card.Body>
        </Card>
        <TrendingTopics userId={user.id} />
      </Container>
    );
  } else {
    return <NotFound />;
  }
}

export default UserProfile;
