import React, { useContext } from "react";
import { DataContext } from "../context/DataContext";
import { useQuery } from "@apollo/client";
import PostCount from "../components/PostsCount";
import TrendingTopics from "../components/TrendingTopics";
import UserList from "../components/UserList";
import { GET_POSTS } from "../queries";
import { Jumbotron, Container, Spinner } from "react-bootstrap";

function Dashboard() {
  const [state, dispatch] = useContext(DataContext);

  useQuery(GET_POSTS, {
    variables: { count: state.count },
    onCompleted: (data) => {
      dispatch({ type: "SET_POSTS", payload: data.allPosts });
    },
  });

  return (
    <>
      <Jumbotron fluid>
        <Container>
          <h1>Dashboard</h1>
          <p>
            See the results of the analysis of relationships between users,
            posts and topics here.
          </p>
        </Container>
      </Jumbotron>
      {state.posts.length === 0 ? (
        <Spinner className={"loading"} animation="border" variant="primary" />
      ) : (
        <Container>
          <PostCount />
          <TrendingTopics />
          <UserList />
        </Container>
      )}
    </>
  );
}

export default Dashboard;
