import React from "react";
import { Container } from "react-bootstrap";

function NotFound() {
  return (
    <Container className={"mt-5"}>
      <h1>Page Not Found</h1>
    </Container>
  );
}

export default NotFound;
