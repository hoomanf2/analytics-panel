import React from "react";
import { Group } from "@vx/group";
import { Bar } from "@vx/shape";
import { scaleLinear, scaleBand } from "@vx/scale";
import { AxisLeft, AxisBottom } from "@vx/axis";

function BarGraph({ width = 700, height = 400, data }: any) {
  const margin = { top: 20, bottom: 55, left: 20, right: 20 };

  const xMax = width - margin.left - margin.right;
  const yMax = height - margin.top - margin.bottom;

  const x = (d: any) => d.label;
  const y = (d: any) => d.repetition;

  const xScale = scaleBand({
    range: [0, xMax],
    round: true,
    domain: data.map(x),
    padding: 0.6,
  });

  const yScale = scaleLinear({
    range: [yMax, 0],
    round: true,
    domain: [0, Math.max(...data.map(y))],
  });

  const compose = (scale: any, accessor: any) => (data: any) =>
    scale(accessor(data));
  const xPoint = compose(xScale, x);
  const yPoint = compose(yScale, y);
  return (
    <svg width={width} height={height}>
      <Group left={50} top={20}>
        <AxisLeft left={10} scale={yScale} numTicks={10} label="Repetition" />
        {data.map((d: any, i: any) => {
          const barHeight = yMax - yPoint(d);
          return (
            <Group key={`bar-${i}`}>
              <Bar
                x={xPoint(d)}
                y={yMax - barHeight}
                height={barHeight}
                width={xScale.bandwidth()}
                fill="#346394"
              />
            </Group>
          );
        })}
        <AxisBottom scale={xScale} label="Topics" labelOffset={15} top={yMax} />
      </Group>
    </svg>
  );
}

export default BarGraph;
