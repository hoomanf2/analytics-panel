import React, { useContext, useEffect } from "react";
import { DataContext } from "../context/DataContext";
import useGetUsers from "../hooks/useGetUsers";
import { Link } from "react-router-dom";
import { Card, Alert, Row, Col } from "react-bootstrap";
import "./UserList.css";

function UserList() {
  const [state, dispatch] = useContext(DataContext);
  const users = useGetUsers(state.posts);

  useEffect(() => {
    if (users.length > 0 && state.users.length === 0) {
      dispatch({ type: "SET_USERS", payload: users });
    }
  });

  return (
    <Card>
      <Card.Body>
        <Card.Title>Users List</Card.Title>
        {users.length === 0 ? (
          <Alert variant={"danger"}>No data found!</Alert>
        ) : (
          <Row md={4} className={"mt-3"}>
            {users.map((user: any) => (
              <Link key={user.id} to={`/user/${user.id}`}>
                <Col className={"mb-4 d-flex"}>
                  <img
                    src={user.avatar}
                    alt={`${user.firstName} ${user.lastName}`}
                    className="avatar"
                  />
                  <div className="name">{`${user.firstName} ${user.lastName}`}</div>
                </Col>
              </Link>
            ))}
          </Row>
        )}
      </Card.Body>
    </Card>
  );
}

export default UserList;
