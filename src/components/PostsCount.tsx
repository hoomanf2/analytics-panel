import React, { useState, useContext } from "react";
import { DataContext } from "../context/DataContext";
import { useLazyQuery } from "@apollo/client";
import { GET_POSTS } from "../queries";
import { Card, Badge, Alert, Form, Col, Button } from "react-bootstrap";

function PostsCount() {
  const [state, dispatch] = useContext(DataContext);
  const [count, setCount] = useState(state.count);

  const [getPosts] = useLazyQuery(GET_POSTS, {
    variables: { count: state.count },
    onCompleted: (data) => {
      dispatch({ type: "SET_POSTS", payload: data.allPosts });
    },
  });

  const changeCount = (e: React.FormEvent<HTMLElement>) => {
    dispatch({ type: "SET_COUNT", payload: count });
    getPosts();
    e.preventDefault();
  };

  return (
    <Card className={"mb-4"}>
      <Card.Body>
        <Card.Title>
          Number of Posts{" "}
          <Badge variant="success">{state.posts.length} Posts</Badge>
        </Card.Title>
        <Alert variant="primary">
          You can get more accurate results by getting more posts, but the speed
          of fetching and processing will be slower.
        </Alert>
        <Form onSubmit={changeCount}>
          <Form.Row className="align-items-center">
            <Col xs="auto">
              <Form.Label htmlFor="post-count" srOnly>
                Number of Posts
              </Form.Label>
              <Form.Control
                type="number"
                className="mb-2"
                id="post-count"
                placeholder="Number of Posts"
                onChange={(e) => setCount(parseInt(e.target.value))}
                defaultValue={count}
              />
            </Col>
            <Col xs="auto">
              <Button type="submit" className="mb-2">
                Get Posts
              </Button>
            </Col>
          </Form.Row>
        </Form>
      </Card.Body>
    </Card>
  );
}

export default PostsCount;
