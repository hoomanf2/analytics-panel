import React, { useState } from "react";
import useGetUserActivity from "../hooks/useGetUserActivity";
import SelectYear from "../components/SelectYear";
import SelectMonth from "../components/SelectMonth";
import AreasGraph from "./AreasGraph";
import { Card, Form, Row, Col } from "react-bootstrap";

function UserActivity({ posts, userId }: any) {
  const [month, setMonth] = useState(0);
  const [year, setYear] = useState(0);

  const [
    allUserPostsCount,
    userPublishedPostsCount,
    userActivityData,
  ] = useGetUserActivity(posts, userId, year, month);

  return (
    <>
      <Form.Row className="align-items-center">
        <Col xs="auto">
          <SelectYear
            onChange={(e: any) => setYear(parseInt(e.target.value))}
          />
        </Col>
        <Col xs="auto">
          <SelectMonth
            onChange={(e: any) => setMonth(parseInt(e.target.value))}
          />
        </Col>
      </Form.Row>

      <Row md={2}>
        <Col md={8}>
          <AreasGraph data={userActivityData} />
        </Col>
        <Col md={4}>
          <Card bg={"info"} text={"light"} className={"mb-5"}>
            <Card.Body>
              <Card.Title>All user posts</Card.Title>
              <Card.Text>
                <h2>{allUserPostsCount}</h2>
              </Card.Text>
            </Card.Body>
          </Card>

          <Card bg={"success"} text={"light"} className={"mb-5"}>
            <Card.Body>
              <Card.Title>Published posts</Card.Title>
              <Card.Text>
                <h2>{userPublishedPostsCount}</h2>
              </Card.Text>
            </Card.Body>
          </Card>

          <Card bg={"danger"} text={"light"}>
            <Card.Body>
              <Card.Title>Unpublished posts</Card.Title>
              <Card.Text>
                <h2>{allUserPostsCount - userPublishedPostsCount}</h2>
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </>
  );
}

export default UserActivity;
