import React, { useState, useContext } from "react";
import { DataContext } from "../context/DataContext";
import useFindTrendingTopics from "../hooks/useFindTrendingTopics";
import SelectYear from "./SelectYear";
import SelectMonth from "./SelectMonth";
import BarGraph from "./BarGraph";
import TrendList from "./TrendList";
import { Card, Form, Row, Col, Alert } from "react-bootstrap";

interface Probs {
  userId?: string | null;
}

const TrendingTopics = React.memo(function TrendingTopics({
  userId = null,
}: Probs) {
  const [state] = useContext(DataContext);
  const [month, setMonth] = useState(0);
  const [year, setYear] = useState(2019);
  const topics = useFindTrendingTopics(state.posts, year, month, userId);

  return (
    <Row className={"mb-4"}>
      <Col>
        <Card>
          <Card.Body>
            <Card.Title>{userId ? "User " : null}Trending Topics</Card.Title>
            <Form.Row className="align-items-center">
              <Col xs="auto">
                <SelectYear
                  onChange={(e: any) => setYear(parseInt(e.target.value))}
                />
              </Col>
              <Col xs="auto">
                <SelectMonth
                  onChange={(e: any) => setMonth(parseInt(e.target.value))}
                />
              </Col>
            </Form.Row>
            {topics.length === 0 ? (
              <Alert variant={"danger"}>No data found!</Alert>
            ) : (
              <BarGraph data={topics} />
            )}
          </Card.Body>
        </Card>
      </Col>
      <Col>
        <TrendList data={topics} userId={userId} />
      </Col>
    </Row>
  );
});

export default TrendingTopics;
