import React from "react";
import { AreaClosed } from "@vx/shape";
import { curveMonotoneX } from "@vx/curve";
import { AxisLeft, AxisBottom } from "@vx/axis";
import { GridRows, GridColumns } from "@vx/grid";
import { Group } from "@vx/group";
import { scaleTime, scaleLinear } from "@vx/scale";
import { extent } from "d3-array";

function AreasGraph({ width = 700, height = 500, data }: any) {
  const getDate = (d: any) => new Date(d.activityMonth);
  const getPostsCount = (d: any): number => d.userPostsCount;

  const margin = { top: 30, right: 80, bottom: 30, left: 30 };

  const xMax = width - margin.left - margin.right;
  const yMax = height - margin.top - margin.bottom;

  const dateScale = scaleTime({
    range: [0, xMax],
    domain: extent(data, getDate) as [Date, Date],
  });

  const dataValueScale = scaleLinear({
    range: [yMax, 0],
    domain: [0, Math.max(...data.map(getPostsCount))],
    nice: true,
  });

  return (
    <svg width={width} height={height}>
      <Group left={70} top={20}>
        <rect
          x={0}
          y={0}
          width={width}
          height={height}
          fill="url(#area-background-gradient)"
          rx={14}
        />

        <GridRows
          scale={dataValueScale}
          width={xMax}
          strokeDasharray="3,3"
          stroke={"#ccc"}
          pointerEvents="none"
        />
        <GridColumns
          scale={dateScale}
          height={yMax}
          strokeDasharray="3,3"
          stroke={"#ccc"}
          pointerEvents="none"
        />

        <AreaClosed
          data={data}
          x={(d) => dateScale(getDate(d))}
          y={(d) => dataValueScale(getPostsCount(d))}
          yScale={dataValueScale}
          strokeWidth={1}
          opacity={0.5}
          fill="#346394"
          curve={curveMonotoneX}
        />

        <AxisLeft
          scale={dataValueScale}
          top={0}
          left={0}
          label={"Posts"}
          stroke={"#1b1a1e"}
        />

        <AxisBottom
          scale={dateScale}
          top={yMax}
          label={"Months"}
          stroke={"#1b1a1e"}
        />
      </Group>
    </svg>
  );
}

export default AreasGraph;
