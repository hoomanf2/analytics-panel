import React from "react";
import { Form } from "react-bootstrap";

function SelectYear({ onChange }: any) {
  let years = [];
  for (let year = new Date().getFullYear(); year >= 2000; year--) {
    years.push(year);
  }
  return (
    <Form.Group>
      <Form.Label srOnly>Select the year</Form.Label>
      <Form.Control as="select" defaultValue={0} onChange={onChange}>
        <option value={0}>All Years</option>
        {years.map((year: number) => (
          <option key={year} value={year}>
            {year}
          </option>
        ))}
      </Form.Control>
    </Form.Group>
  );
}

export default SelectYear;
