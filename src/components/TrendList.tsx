import React from "react";
import { Card, Alert, ListGroup, Badge } from "react-bootstrap";

function compare_repetition(a: any, b: any) {
  if (a.repetition > b.repetition) {
    return -1;
  } else if (a.repetition < b.repetition) {
    return 1;
  } else {
    return 0;
  }
}

function TrendList({ data, userId }: any) {
  data.sort(compare_repetition);
  return (
    <Card>
      <Card.Header><b>{userId ? "User " : null}Trend List</b></Card.Header>
      {data.length === 0 ? (
        <Card.Body>
          <Alert variant={"danger"}>No data found!</Alert>
        </Card.Body>
      ) : (
        <ListGroup variant="flush">
          {data.map((topic: any, index: number) => (
            <ListGroup.Item key={topic.label}>
              <b>{index + 1}.</b> {topic.label}{" "}
              <Badge variant="info">{topic.repetition}</Badge>
            </ListGroup.Item>
          ))}
        </ListGroup>
      )}
    </Card>
  );
}

export default TrendList;
